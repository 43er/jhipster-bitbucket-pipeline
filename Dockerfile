# Docker image to be used with jhipster (>= version 4)
# with maven and gulp

# use openjdk-8 and maven 3
FROM  maven:3-jdk-8

RUN \

  apt-get update && \
  apt-get install -y apt-utils && \
  apt-get install -y curl build-essential && \

  # install node
  curl -sL https://deb.nodesource.com/setup_6.x | bash - && \
  apt-get install -y nodejs && \

  # install yarn
  npm install -g yarn && \

  # for python deployment
  apt-get install -y python3 python3-pip && \
  pip3 install boto3==1.3.0 && \

  # cleanup
  apt-get clean && \
  rm -rf \
    /var/lib/apt/lists/* \
    /tmp/* \
    /var/tmp/* && \

  echo "finished"


CMD ["bash"]
