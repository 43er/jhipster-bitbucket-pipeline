# jhipster-bitbucket-pipeline
## Bitbucket JHipster pipeline Docker image Dockerfile

this repo was forked from https://github.com/laszlomiklosik/jhipster-bitbucket-pipeline 
and modified for my needs (mvn, gulp, node version).

Example usage in bitbucket-pipelines.yml file:

```
image: cdaller/jhipster-bitbucket-pipeline:jhipster4.x

pipelines:
  default:
    - step:
        script:
          - yarn install
          # compie and unit tests:
          - mvn clean package -DskipITs
          # run ITs
          - mvn verify
          # build package in prod mode
          - mvn clean package -DskipITs -Pprod
```


Note: this Dockerfile is built after each commit to 'all branches'.

Docker image info: https://hub.docker.com/r/cdaller/jhipster-bitbucket-pipeline/
